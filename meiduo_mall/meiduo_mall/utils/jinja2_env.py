from jinja2 import Environment
from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import reverse


def jinja2_environment(**options):
    """jinja2环境"""

    # 创建环境对象
    env = Environment(**options)

    # 自定义语法：{{static:静态文件路径}} {{url:路由的命名空间}}
    env.globals.update({
        'static': staticfiles_storage.url, # 获取静态文件的前缀
        'url': reverse, # 反向解析
    })

    # 返回环境对象
    return env


"""
模板里面有些语句不是那么好用的，比如读取静态文件，重定向等等，那么就需要自定义一些配置
确保可以使用模板引擎中的{{ url('') }} {{ static('') }}这类语句 
"""
